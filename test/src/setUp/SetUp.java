package setUp;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class SetUp {
	public WebDriver driver=null;
  @BeforeTest
  public void Setup() {
	  System.setProperty("webdriver.chrome.driver", "D:\\CTU\\file\\chromedriver.exe");
	  driver=new ChromeDriver();
	 driver.manage().timeouts().pageLoadTimeout(40,TimeUnit.SECONDS);
	 driver.manage().window().maximize();	  
  }
  @AfterTest
  public void TearDown() throws InterruptedException {
	  Thread.sleep(4000);
	  driver.quit();
  } 
}
